package ar.edu.untref.aydoo.unit;

import ar.edu.untref.aydoo.CharCounter;
import org.junit.Test;
import org.junit.Assert;

import java.util.HashMap;

public class CounterTest {

    @Test
    public void pruebaVacio() {
        CharCounter counter = new CharCounter();
        HashMap<Character, Integer> res = counter.count("");
        Assert.assertEquals(0, res.size());
    }

}
