package ar.edu.untref.aydoo.unit;

import org.junit.Test;
import org.junit.Assert;

public class StringTest {

    @Test
    public void devuelveTrueCuandoLaLongitudDelStringCoincide() {
        String string = "123";
        int size = string.length();
        Assert.assertEquals(3, size);
    }

    @Test
    public void devuelveFalseCuandoLaLongitudDelStringNoCoincide() {
        String string = "123";
        int size = string.length();
        Assert.assertNotEquals(5, size);
    }

    @Test
    public void devuelveTrueCuandoLaPrimerLetraDelStringCoincideConA() {
        String string = "abcd";
        string = string.concat("efgh");
        Assert.assertTrue(string.startsWith("a"));
    }

    @Test
    public void devuelveFalseCuandoLaPrimerLetraDelStringNoCoincideConE() {
        String string = "abcd";
        string = string.concat("efgh");
        Assert.assertFalse(string.startsWith("e"));
    }

    @Test
    public void devuelveTrueCuandoLaUltimaLetraDelStringCoincideConA() {
        String string = "abcd";
        string = string.concat("efgh");
        Assert.assertTrue(string.endsWith("h"));
    }

    @Test
    public void devuelveFalseCuandoLaUltimaLetraDelStringNoCoincideConE() {
        String string = "abcd";
        string = string.concat("efgh");
        Assert.assertFalse(string.endsWith("a"));
    }

}
