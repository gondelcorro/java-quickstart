#!/bin/bash

set -e

mvn clean test -Djdk.net.URLClassPath.disableClassPathURLCheck=true